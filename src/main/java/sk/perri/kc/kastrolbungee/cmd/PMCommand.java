package sk.perri.kc.kastrolbungee.cmd;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import sk.perri.kc.kastrolbungee.Main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class PMCommand extends Command implements TabExecutor
{
    public PMCommand()
    {
        super("pm");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if(sender.hasPermission("kb.pm") && args.length > 1)
        {
            ProxiedPlayer p = Main.self.getProxy().getPlayer(args[0]);
            if(p == null)
            {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Main.config.getString("pm-notfound")));

                return;
            }

            TextComponent msg = new TextComponent(ChatColor.translateAlternateColorCodes('&',
                    Main.config.getString("pm-format").replace("%sender%",
                            sender.getName()).replace("%reciever%", p.getName()).replace("%sprava%",
                            String.join(" ", Arrays.copyOfRange(args, 1, args.length)))));

            p.sendMessage(msg);
            sender.sendMessage(msg);
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args)
    {
        List<String> plrs = new ArrayList<>();

        Main.self.getProxy().getPlayers().forEach(p ->
        {
            if(args.length == 0 || p.getName().toLowerCase().startsWith(args[args.length-1].toLowerCase()))
                plrs.add(p.getName());
        });

        return plrs;
    }
}
