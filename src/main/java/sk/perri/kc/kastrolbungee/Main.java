package sk.perri.kc.kastrolbungee;

import com.google.common.io.ByteStreams;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import sk.perri.kc.kastrolbungee.cmd.AdminChatCommand;
import sk.perri.kc.kastrolbungee.cmd.PInfoCommand;
import sk.perri.kc.kastrolbungee.cmd.PMCommand;

import java.io.*;

public class Main extends Plugin
{
    public static Main self;
    public static Configuration config;

    @Override
    public void onEnable()
    {
        self = this;
        if(!getDataFolder().exists())
            getDataFolder().mkdir();

        File configFile = new File(getDataFolder(), "config.yml");
        if (!configFile.exists())
        {
            try
            {
                configFile.createNewFile();
                try (InputStream is = getResourceAsStream("config.yml");
                     OutputStream os = new FileOutputStream(configFile))
                {
                    ByteStreams.copy(is, os);
                }
            }
            catch (IOException e) {
                throw new RuntimeException("Unable to create configuration file", e);
            }
        }

        try
        {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(configFile);
        }
        catch (IOException e)
        {
            getLogger().warning("[E] Neviem nacitat config!");
        }
        getProxy().getPluginManager().registerCommand(this, new AdminChatCommand());
        getProxy().getPluginManager().registerCommand(this, new PMCommand());
        getProxy().getPluginManager().registerCommand(this, new PInfoCommand());
        getLogger().info("[I] Plugin sa nacital");
    }
}
