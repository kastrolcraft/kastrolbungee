package sk.perri.kc.kastrolbungee.cmd;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import sk.perri.kc.kastrolbungee.Main;

import java.util.ArrayList;
import java.util.List;

public class PInfoCommand extends Command implements TabExecutor
{
    public PInfoCommand()
    {
        super("pinfo");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if(args.length == 1 && sender.hasPermission("kb.pinfo"))
        {
            ProxiedPlayer p = Main.self.getProxy().getPlayer(args[0]);
            if(p == null)
            {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&',
                        Main.config.getString("pm-notfound")));
                return;
            }

            Main.config.getStringList("pinfo").forEach(l ->
            {
                sender.sendMessage(ChatColor.translateAlternateColorCodes('&', l.replace("%nick%",
                        p.getName()).replace("%ip%", p.getAddress().getHostString()).replace("%server%",
                        p.getServer().getInfo().getName()).replace("%ping%",  Integer.toString(p.getPing()))));
            });
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args)
    {
        List<String> plrs = new ArrayList<>();

        Main.self.getProxy().getPlayers().forEach(p ->
        {
            if(args.length == 0 || p.getName().toLowerCase().startsWith(args[args.length-1].toLowerCase()))
                plrs.add(p.getName());
        });

        return plrs;
    }
}
