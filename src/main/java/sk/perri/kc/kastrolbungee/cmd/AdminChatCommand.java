package sk.perri.kc.kastrolbungee.cmd;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;
import net.md_5.bungee.api.plugin.TabExecutor;
import sk.perri.kc.kastrolbungee.Main;

import java.util.ArrayList;
import java.util.List;

public class AdminChatCommand extends Command implements TabExecutor
{
    public AdminChatCommand()
    {
        super("a");
    }

    @Override
    public void execute(CommandSender sender, String[] args)
    {
        if(sender.hasPermission("kb.adminchat") && args.length > 0)
        {

            TextComponent msg = new TextComponent(ChatColor.translateAlternateColorCodes('&',
                    Main.config.getString("adminchat-format").replace("%nick%",
                            sender.getName()).replace("%sprava%", String.join(" ", args))));

            Main.self.getProxy().getPlayers().forEach(p ->
            {
                if(p.isConnected() && p.hasPermission("kb.adminchat"))
                {
                    p.sendMessage(msg);
                }
            });
        }
    }

    @Override
    public Iterable<String> onTabComplete(CommandSender sender, String[] args)
    {
        List<String> plrs = new ArrayList<>();

        Main.self.getProxy().getPlayers().forEach(p ->
        {
            if(args.length == 0 || p.getName().toLowerCase().startsWith(args[args.length-1].toLowerCase()))
                plrs.add(p.getName());
        });

        return plrs;
    }
}
